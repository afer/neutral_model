import matplotlib
matplotlib.use('Agg')

import sys
import math
import pandas as pd
import numpy as np
import re
import os


from lmfit import Parameters, Model, fit_report
from scipy.stats import beta
from statsmodels.stats.proportion import proportion_confint
from pysurvey import rarefy
from matplotlib import pyplot
from adjustText import adjust_text

#Extract parameters from simulation file
source_dir = os.path.dirname(os.path.realpath(__file__))

params_dic = {}

with open(source_dir+"/../output/params.txt", 'r') as f:
    for line in f:
        if re.search('(\S+)\s(true|false|[-+]?[0-9]+[\.e]?[0-9]+)',line):
            line_data = re.search('(\S+)\s(true|false|[-+]?[0-9]+[\.e]?[0-9]+)',line)
            params_dic[line_data.group(1)] = line_data.group(2) 
          

    f.close()

params_dic

shortnames = {'t_max':'Tm','host_generation_time':'Thg','microbiome_heritability':'Her'}

shortnames['reproduction_additivity'] = 'RA' if 'reproduction_additivity' == 'true' else'RB'
shortnames['survival_additivity'] = 'SA' if 'survival_additivity' == 'true' else 'SB'

params_dic['reproduction_additivity'] = params_dic['reproduction_impact']
params_dic['survival_additivity'] = params_dic['survival_impact']

param_sentence = ''
for param in shortnames.keys():
    param_sentence += shortnames[param] + '=' + params_dic[param] + ' '


# truncated cumulative beta-distribution function as predicted by neutral model
def beta_cdf(p, N, m):
    return beta.cdf(1.0, N*m*p, N*m*(1.0-p)) - beta.cdf(1.0/N, N*m*p, N*m*(1.0-p))

# command line arguments
remove_rare_otus = float(sys.argv[1])       # remove otus that have less than this number of reads (default 0)
rarefy_to = int(sys.argv[2])                # rarefy reads to this depth
n_nonneutral_otus = int(sys.argv[3])        # how many non-neutral otus should be labeled in plot?
data_files = sys.argv[4:len(sys.argv)]    # list of data files with raw read counts

if remove_rare_otus < 0:
    remove_rare_otus = 0

#Read individual-experiments data


times_vector = []

for counts_file in data_files:
    print(counts_file)
    print(source_dir+'/../output/otutable_sampled_(t.*).csv')
    sim_time = re.search('otutable_sampled_(t.*)',counts_file).group(1)
    # reading data
    print('processing ' + counts_file)
    raw_counts = pd.io.parsers.read_table(counts_file, header=0, index_col=0)
    raw_counts = raw_counts[raw_counts.sum(1) > remove_rare_otus]
    raw_counts = raw_counts.loc[:, raw_counts.sum(0) > 0.5*np.median(raw_counts.sum(0))]

    n_raw_otus, n_raw_samples = raw_counts.shape
    print('#samples=' + str(n_raw_samples), ', #otus=' + str(n_raw_otus))
    print(raw_counts.sum(0))


    # rarefy to given read depth
    if rarefy_to < 0:
        rarefy_to = min(raw_counts.sum(0))
    rarefied_counts = (rarefy(raw_counts.transpose(), rarefy_to, replace=False, remove_shallow=True)).transpose()
    rarefied_counts = rarefied_counts[rarefied_counts.sum(1) > 0]

    n_otus, n_samples = rarefied_counts.shape
    n_reads = np.mean(rarefied_counts.sum(0))
    if n_samples < 5:
        print('not enough samples in ' + counts_file)
        break

    # calculate abundance and occurence data from reads
    mean_relative_abundance = 1.0*rarefied_counts.sum(1)/n_reads/n_samples
    occurence_frequency = 1.0*np.count_nonzero(rarefied_counts, axis=1)/n_samples

    abundance_occurence_data = pd.DataFrame(mean_relative_abundance)
    abundance_occurence_data.columns = ['mean_rel_abu']
    abundance_occurence_data['occurence_freq'] = occurence_frequency
    abundance_occurence_data = abundance_occurence_data.sort_values(by=['mean_rel_abu'])

    # fit the neutral model
    params = Parameters()
    params.add('N', value=n_reads, vary=False)
    params.add('m', value=0.5, min=0.0, max=1.0)
    beta_model = Model(beta_cdf)
    beta_fit = beta_model.fit(abundance_occurence_data['occurence_freq'], params, p=abundance_occurence_data['mean_rel_abu'])

    # best fit statistics
    r_squares_neutral = 1.0 - beta_fit.residual.var()/np.var(abundance_occurence_data['occurence_freq'])
    aic_neutral = beta_fit.aic + 4.0/n_samples
    print(fit_report(beta_fit))
    print('R^2 = ' + str(r_squares_neutral) + ', ' + 'AIC = ' + str(aic_neutral))

    # adding neutral prediction to dataframe
    abundance_occurence_data['predicted_freq'] = beta_fit.best_fit
    abundance_occurence_data['lower_conf_int'], abundance_occurence_data['upper_conf_int'] = proportion_confint(abundance_occurence_data['predicted_freq']*n_samples, n_samples, alpha=0.05, method='wilson')

    # determine non-neutral otus
    otu_above_neutral = abundance_occurence_data[np.round(abundance_occurence_data['occurence_freq'], 5) > np.round(abundance_occurence_data['upper_conf_int'], 5)]
    otu_below_neutral = abundance_occurence_data[np.round(abundance_occurence_data['occurence_freq'], 5) < np.round(abundance_occurence_data['lower_conf_int'], 5)]
    abundance_occurence_data['neutral_offset'] = np.where(np.round(abundance_occurence_data['occurence_freq'], 5) > np.round(abundance_occurence_data['upper_conf_int'], 5), abundance_occurence_data['occurence_freq'] - abundance_occurence_data['upper_conf_int'], np.where(np.round(abundance_occurence_data['occurence_freq'], 5) < np.round(abundance_occurence_data['lower_conf_int'], 5), abundance_occurence_data['lower_conf_int'] - abundance_occurence_data['occurence_freq'], 0))

    # plotting from here
    pyplot.xscale('log')
    pyplot.xlim(0.75*min(abundance_occurence_data['mean_rel_abu']), 1.25*max(abundance_occurence_data['mean_rel_abu']))
    pyplot.ylim(-0.05, 1.05)

    pyplot.plot(abundance_occurence_data['mean_rel_abu'], abundance_occurence_data['occurence_freq'], 'o', markersize=10, fillstyle='full', color='black')
    pyplot.plot(otu_above_neutral['mean_rel_abu'], otu_above_neutral['occurence_freq'], 'o', markersize=10, fillstyle='full', color='red')
    pyplot.plot(otu_below_neutral['mean_rel_abu'], otu_below_neutral['occurence_freq'], 'o', markersize=10, fillstyle='full', color='blue')
    if 'OTU 1' in abundance_occurence_data.index.values:
        pyplot.plot(abundance_occurence_data['mean_rel_abu']['OTU 1'], abundance_occurence_data['occurence_freq']['OTU 1'], 'o', markersize=10, fillstyle='full', color='green')

    pyplot.text(min(abundance_occurence_data['mean_rel_abu']), 1.08,sim_time , horizontalalignment='center', verticalalignment='center', fontsize=9)
    pyplot.text(min(abundance_occurence_data['mean_rel_abu']), 1.0,param_sentence, horizontalalignment='left', verticalalignment='center', fontsize=9)

    pyplot.plot(abundance_occurence_data['mean_rel_abu'], abundance_occurence_data['predicted_freq'], '-', lw=5, color='darkred')
    pyplot.plot(abundance_occurence_data['mean_rel_abu'], abundance_occurence_data['lower_conf_int'], '--', lw=2, color='darkred')
    pyplot.plot(abundance_occurence_data['mean_rel_abu'], abundance_occurence_data['upper_conf_int'], '--', lw=2, color='darkred')
    pyplot.fill_between(abundance_occurence_data['mean_rel_abu'], abundance_occurence_data['lower_conf_int'],abundance_occurence_data['upper_conf_int'], color='lightgrey')

    # n 'most non-neutral' otus with labels or not?
    #if n_nonneutral_otus > 0:
    #    non_neutral_otus = abundance_occurence_data.nlargest(n_nonneutral_otus, 'neutral_offset').index
    #    otu_labels = []
    #    for index, otu in abundance_occurence_data.iterrows():
    #        if index in non_neutral_otus:
    #            otu_labels.append(pyplot.text(otu['mean_rel_abu'], otu['occurence_freq'], index))
    #        else:
    #            otu_labels.append(pyplot.text(otu['mean_rel_abu'], otu['occurence_freq'], ''))
    #    pyplot.title(str(adjust_text(otu_labels, arrowprops=dict(arrowstyle="-", color='k', lw=0.5), force_points=0.5)))

    pyplot.title('R^2 = ' + '{:1.2f}'.format(np.mean(r_squares_neutral)))
    pyplot.xlabel('Mean relative abundance across samples', fontsize=18)
    pyplot.ylabel('Occurence frequency in samples', fontsize=18)

    pyplot.savefig(counts_file + '_neutral_fit.jpg')

    pyplot.clf()
