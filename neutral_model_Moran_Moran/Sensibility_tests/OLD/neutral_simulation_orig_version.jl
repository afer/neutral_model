#Julia original version of neutral_simulation.py script

#   Main differences between Python and Julia code:
#	- Libraries StatsBase (For weighted and non-weighted sampling) and Distributions (For obtaining the sampling distributions)
#   - Loops: In julia you can use ** for i=1:n_elements ** instead of ** for i in range(len(elements_vector)) **
#   - Declaring type in function: It can speed up running time if you tell the function which datatype to expect (It is not indispensable though)
#   - string() instead of str()
#   - Sampling functions explained below

#   Main similitudes to Python
#   - Overall language
#   - Comprehensive Lists (In julia the conditionals inside comprehension are very limited)
#   - Pretty much everything else. I'll point some changes I made in the code with this '###'

using StatsBase,Distributions

const n_hosts=50                         # number of hosts
const n_microbes=10000                   # number of individuals (microbes) per host
const n_microbial_species=200            # number of species
const host_reproduction = true           # do hosts also reproduce? "false" corresponds to the standard neutral model ### true and false instead of True and False
const host_generation_time = 10000       # generation time of hosts
const n_initial_colonizers = 5           # how many microbial species colonize a host from the environment?
const microbiome_heritability = 0.5      # what fraction of the microbiome is inherited?
const m=0.01                             # immigration probability of the neutral model

const t_max=10000                     # how long does the simulation run
const t_sample = 10000                   # how often to take a microbiome sample

const n_samples = Int(0.04*n_microbes)   # what is the sample size

# function takes a range of species and their distribution in an environment and returns a microbiome
function colonize_from_environment(species::Array{Int}, n_microbes::Int, environment::Array{Float64}, n_colonizers::Int)
    microbiome = zeros(Int,length(species))  ### Type and length are inverted

    init_species = wsample(species, environment, n_colonizers)  ### The parameters in wsample are in a different position compared to python's function
    for i = 1:length(init_species)-1
        if n_microbes - sum(microbiome) > 1
            microbiome[init_species[i]] += sample(1:n_microbes - sum(microbiome))
        end
    end
    microbiome[init_species[length(init_species)-1]] += n_microbes - sum(microbiome)

    return microbiome
end

# define array of the species (basically just a list 0, ..., n)
microbial_species = [Int(i) for i=1:n_microbial_species]
otu_id = ["OTU " * string(i) for i in microbial_species]

# define the source community of the neutral model
source_community = rand(Geometric(1e-5),n_microbial_species)
source_community = 1.0*source_community/sum(source_community)

# define the hosts
hosts = [colonize_from_environment(microbial_species, n_microbes, source_community, n_initial_colonizers) for i=1:n_hosts]
host_id = ["Host" * string(i) for i = 1:n_hosts]

# run the simulation
for t = 1:t_max

    # host reproduction
    if host_reproduction && t % host_generation_time == 0
        # pick a host to die and one to reproduce
        host_to_die, host_to_reproduce= sample(1:n_hosts,2,replace=false)   ### Changed this from two split processes to one without replacement. Will have to separate it again for adding advantages.

        # determine microbiome for new host as weighted sum of environmental and ancestral microbes
        env_sample = 1.0*colonize_from_environment(microbial_species, n_microbes, source_community, n_initial_colonizers)/n_microbes
        inherited_microbes = wsample(microbial_species, (1.0*microbiome_heritability*hosts[host_to_reproduce]/n_microbes + (1.0-microbiome_heritability)*env_sample),n_microbes)
        
        hosts[host_to_die] = zeros(Int,n_microbial_species)
        for species in inherited_microbes
            hosts[host_to_die][species] += 1
        end
    end

    # microbial reproduction
    for microbiome in hosts
        # death
        microbiome[wsample(microbial_species, 1.0*microbiome/n_microbes)] -= 1
        # birth
        microbiome[wsample(microbial_species, (m*source_community + (1-m)*microbiome/(n_microbes-1)))] += 1
    end
    
    # collect microbiome sample
    if t % t_sample == 0
        micro_sample = zeros(Int,n_hosts, n_microbial_species)
        for i =1:n_hosts
            rel_freq = 1.0*hosts[i]/n_microbes
            samples = wsample(microbial_species, rel_freq,n_samples)
            for j in samples
                micro_sample[i,j] += 1
            end
        end
        
        # write otu table to file
        writedlm("output/otutable_t" * lpad(t-t_sample,10,0), transpose(micro_sample))  ### lpad is for adjusting the number of digits.
    end
end
