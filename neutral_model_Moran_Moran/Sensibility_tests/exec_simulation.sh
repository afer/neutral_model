#Bash script to generate organized simulations.
DATE=$(date +%Y%m%d%H%M) # Date and time when the simulation was made
EXP_NAME="HOST_SINGLE_MICROBE_EFFECT_TRACK" # Name of the general experiment
EXP_VARIABLE="HGT=1e2_Her=1_RA=1_Tm=1e9_LongTimeExperiment" # Name of the variables of interest -- R:Reprodution D:Death B: Boolean A:Additive - HGT: Host Generation Time - Her: Heritability - Tm: Time of experiment

WDIR="runs/${EXP_NAME}_${DATE}_${EXP_VARIABLE}"
mkdir $WDIR
mkdir "$WDIR/output"
mkdir "$WDIR/performance"
mkdir "$WDIR/plots"
mkdir "$WDIR/notebooks"
cp -r source $WDIR
cp notebooks/Abundance_Analysis.ipynb $WDIR/notebooks
cp notebooks/Plot_arranged.ipynb $WDIR/notebooks
cp rerun.sh $WDIR

cd $WDIR

echo "Starting simulation"
/usr/bin/time -o "performance/neut_sim_time.txt" julia source/neutral_simulation.jl

echo "Simulation finished. Running notebooks"
jupyter nbconvert --execute --to notebook --inplace notebooks/Plot_arranged.ipynb
 
echo "Notebooks ran. Starting Plotting."

python2 source/neutral_fit.py 0 400 1 output/otutable_sampled_t*.csv
echo "Plotting finished. Files in $WDIR/output"