#Bash script to rerun a previously ran simulation.

echo "Starting simulation"
/usr/bin/time -o "PERFORMANCE/neut_sim_time.txt" julia source/neutral_simulation.jl

echo "Simulation finished.Starting Plotting."

python2 source/neutral_fit.py 0 400 1 output/otutable_sampled_t*.csv

echo "Plotting finished. Files in $WDIR/OUTPUT"
