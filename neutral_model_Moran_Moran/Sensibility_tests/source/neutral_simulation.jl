using StatsBase,Distributions,NamedArrays

seed = 3 #"NA" # seed for repeated initial conditions
srand(seed)

cur_dir = pwd()

const n_hosts = Int(5e1)                      # number of hosts
const n_microbes = Int(1e4)                # number of individuals (microbes) per host
const n_microbial_species = Int(2e2)         # number of species
const host_reproduction = true          # do hosts also reproduce? "false" corresponds to the standard neutral model
const host_generation_time = 1e2     # generation time of hosts
const n_initial_colonizers = 5          # how many microbial species colonize a host from the environment?
const microbiome_heritability = 1.     # what fraction of the microbiome is inherited?
const m = 0.01                            # immigration probability of the neutral model

const t_max = 1e9                   # how long does the simulation run
const t_sample = host_generation_time #1e4  # how often to take a microbiome sample NOTE: IF (t_sample != HGT) YOU MUST ADJUST Plotter_arranged.ipynb

const n_samples = Int(0.04*n_microbes) # what is the sample size

const reproduction_additivity = true   # multiple bacteria of the effector type will keep affecting reproduction if true. If this is true reproduction impact should be very low.
const survival_additivity = false       # multiple bacteria of the effector type will keep affecting death if true. If this is true survival impact should be very low.
const survival_impact = 0.          # (alpha_s) Impact of the reproduction in the selection [value between -1 and 1] * If -1 the host is less likely to die. If +1 is more likely to do so
const reproduction_impact = 1.      #(alpha_r) Impact of the reproduction in the selection [value between -1 and 1]
const effector_microbe = 1              # which microbe has an effect in the host

param_names = ["n_hosts","n_microbes","n_microbial_species","host_reproduction","host_generation_time",
    "n_initial_colonizers","microbiome_heritability", "m", "t_max","t_sample","n_samples","reproduction_additivity",
    "survival_additivity","survival_impact","reproduction_impact","effector_microbe","seed"]
param_values = [n_hosts,n_microbes,n_microbial_species,host_reproduction,host_generation_time,
    n_initial_colonizers,microbiome_heritability, m, t_max,t_sample,n_samples,reproduction_additivity,
    survival_additivity,survival_impact,reproduction_impact,effector_microbe,seed]

writedlm("$cur_dir/output/params.txt",[param_names map(x->string(x),param_values)])



death_rep_vector = Array{Int64}[]

# function takes a range of species and their distribution in an environment and returns a microbiome
function colonize_from_environment(species::Array{Int64}, n_microbes::Int64, environment::Array{Float64}, n_colonizers::Int64)
    microbiome = zeros(Int64,length(species))

    init_species = wsample(species, environment, n_colonizers)
    for i = 1:length(init_species)-1
        if n_microbes - sum(microbiome) > 1
            microbiome[init_species[i]] += sample(1:n_microbes - sum(microbiome))
        end
    end
    microbiome[init_species[length(init_species)]] += n_microbes - sum(microbiome)

    return microbiome
end

# function takes the impact of one strain, if this impact is additive, the index of this strain and the state of the system and returns a weight vector
function set_host_weight_vector(effect_value::Float64,additivity::Bool, effector_microbe::Int64,hosts_matrix::Array{Int64,2})
        new_n_hosts = size(hosts_matrix)[2]

        effector_microbes_per_host_w =  effect_value > 0 ? hosts_matrix[effector_microbe,:] : n_microbes - hosts_matrix[effector_microbe,:]
        total_effectors_w = sum(effector_microbes_per_host_w)

        if sum(total_effectors_w) > 0    #This condition is for the case where there are no effector microbes.
            effect_type = additivity == true ? effector_microbes_per_host_w :  effector_microbes_per_host_w .!= 0
            abs_effect_value = abs(effect_value)
            hosts_weight_vector = (1. / new_n_hosts) * (1. - abs_effect_value) + abs_effect_value * (effect_type / total_effectors_w )
        else
            hosts_weight_vector = ones(new_n_hosts) / new_n_hosts
        end

    return hosts_weight_vector
end

# define array of the species (basically just a list 0, ..., n)
microbial_species = [Int(i) for i=1:n_microbial_species]
otu_id = ["OTU " * string(i) for i in microbial_species]

# define the source community of the neutral model
source_community = rand(Geometric(1e-5),n_microbial_species)
source_community = 1.0*source_community/sum(source_community)

#Write source community
writedlm("$cur_dir/output/source_community.csv",source_community,"\t")

# define the hosts
hosts = Matrix{Int64}(n_microbial_species,n_hosts)
host_vec = Vector{Int64}(n_hosts)
host_id = Array{String,1}(n_hosts)

for host_num=1:n_hosts
    hosts[:,host_num] = colonize_from_environment(microbial_species, n_microbes, source_community, n_initial_colonizers)
    host_vec[host_num] = host_num
    host_id[host_num] = "Host " * string(host_num)
end

#Save initial state

micro_sample = zeros(Int, n_microbial_species,n_hosts)
for host_num in host_vec
    rel_freq = 1.0*hosts[:,host_num]/n_microbes
    samples = wsample(microbial_species, rel_freq,n_samples)
    for j in samples
        micro_sample[j,host_num] += 1
    end
end

# write otu table to file
header = "\t"
for hostname in host_id
    header *= hostname*"\t"
end
header *= "\n"

# write whole state
f = open("$cur_dir/output/otutable_whole_t" * lpad(0,10,0) * ".csv","w")
write(f,header)
writedlm(f, NamedArray(hosts, (otu_id, host_id ), ("OTU","TAXA")))
close(f)

# write sample
f = open("$cur_dir/output/otutable_sampled_t" * lpad(0,10,0) * ".csv","w")
write(f,header)
writedlm(f, NamedArray(micro_sample, (otu_id, host_id ), ("OTU","TAXA")))
close(f)


# run the simulation
for t = 1:t_max

    # host reproduction
    if host_reproduction && t % host_generation_time == 0

        # pick a host to die and one to reproduce
        host_to_die = survival_impact == 1 ? sample(host_vec): wsample(host_vec,set_host_weight_vector(survival_impact,survival_additivity,effector_microbe,hosts))
        host_to_reproduce = reproduction_impact == 0 ? sample(host_vec[1:end .!= host_to_die]): wsample(host_vec[1:end .!= host_to_die],
        set_host_weight_vector(reproduction_impact,reproduction_additivity,effector_microbe,hosts[:,1:end .!= host_to_die]))

        #Insert data into history vector
        push!(death_rep_vector,[host_to_die,host_to_reproduce])

        # determine microbiome for new host as weighted sum of environmental and ancestral microbes
        env_sample = 1.0*colonize_from_environment(microbial_species, n_microbes, source_community, n_initial_colonizers)/n_microbes
        inherited_microbes = wsample(microbial_species, (1.0*microbiome_heritability*hosts[:,host_to_reproduce]/n_microbes + (1. - microbiome_heritability)*env_sample),n_microbes)

        hosts[:,host_to_die] = zeros(Int,n_microbial_species)
        for species in inherited_microbes
            hosts[species,host_to_die] += 1
        end
    end

    # microbial reproduction
    for host_index= 1:n_hosts
        # death
        hosts[wsample(microbial_species, 1.0*hosts[:,host_index]/n_microbes),host_index] -= 1
        # birth
        hosts[wsample(microbial_species, (m*source_community + (1-m)*hosts[:,host_index]/(n_microbes-1))),host_index] += 1
    end
    # collect microbiome sample

    if t % t_sample == 0
        
        # we save the current state and a sample from it
        
        micro_sample = zeros(Int, n_microbial_species,n_hosts)
        for host_num in host_vec
            rel_freq = 1.0*hosts[:,host_num]/n_microbes
            samples = wsample(microbial_species, rel_freq,n_samples)
            for j in samples
                micro_sample[j,host_num] += 1
            end
        end

        # write otu table to file
        header = "\t"
        for hostname in host_id
            header *= hostname*"\t"
        end
        header *= "\n"
    
        # write whole state
        f = open("$cur_dir/output/otutable_whole_t" * lpad(Int(t),10,0) * ".csv","w")
        write(f,header)
        writedlm(f, NamedArray(hosts, (otu_id, host_id ), ("OTU","TAXA")))
        close(f)
        
        # write sample
        f = open("$cur_dir/output/otutable_sampled_t" * lpad(Int(t),10,0) * ".csv","w")
        write(f,header)
        writedlm(f, NamedArray(micro_sample, (otu_id, host_id ), ("OTU","TAXA")))
        close(f)
    end
end


writedlm("$cur_dir/output/death_reproduction_history", death_rep_vector)
