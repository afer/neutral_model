
using StatsBase,Distributions,NamedArrays

seed = "NA" # seed for repeated initial conditions
#srand(seed)
 
cur_dir = pwd()

const n_hosts = 5                      # number of hosts
const n_microbes = 100                # number of individuals (microbes) per host
const n_microbial_species = 10         # number of species
const host_reproduction = true          # do hosts also reproduce? "false" corresponds to the standard neutral model
const host_generation_time = 100     # generation time of hosts
const n_initial_colonizers = 5          # how many microbial species colonize a host from the environment?
    # what fraction of the microbiome is inherited?
const m = 0.                            # immigration probability of the neutral model

const t_max = 1e7                   # how long does the simulation run

const reproduction_additivity = true   # multiple bacteria of the effector type will keep affecting reproduction if true. If this is true reproduction impact should be very low.
const reproduction_impact = 1.      #(alpha_r) Impact of the reproduction in the selection [value between -1 and 1]
const effector_microbe = 1              # which microbe has an effect in the host

# function takes a range of species and their distribution in an environment and returns a microbiome
function colonize_from_environment(species::Array{Int64}, n_microbes::Int64, environment::Array{Float64}, n_colonizers::Int64)
    microbiome = zeros(Int64,length(species))

    init_species = wsample(species, environment, n_colonizers)
    for i = 1:length(init_species)-1
        if n_microbes - sum(microbiome) > 1
            microbiome[init_species[i]] += sample(1:n_microbes - sum(microbiome))
        end
    end
    microbiome[init_species[length(init_species)]] += n_microbes - sum(microbiome)

    return microbiome
end

# function takes the impact of one strain, if this impact is additive, the index of this strain and the state of the system and returns a weight vector
function set_host_weight_vector(effect_value::Float64,additivity::Bool, effector_microbe::Int64,hosts_matrix::Array{Int64,2})
        new_n_hosts = size(hosts_matrix)[2]

        effector_microbes_per_host_w =  effect_value > 0 ? hosts_matrix[effector_microbe,:] : n_microbes - hosts_matrix[effector_microbe,:]
        total_effectors_w = sum(effector_microbes_per_host_w)

        if sum(total_effectors_w) > 0    #This condition is for the case where there are no effector microbes.
            effect_type = additivity == true ? effector_microbes_per_host_w :  effector_microbes_per_host_w .!= 0
            abs_effect_value = abs(effect_value)
            hosts_weight_vector = (1. / new_n_hosts) * (1. - abs_effect_value) + abs_effect_value * (effect_type / total_effectors_w )
        else
            hosts_weight_vector = ones(new_n_hosts) / new_n_hosts
        end

    return hosts_weight_vector
end

# define array of the species (basically just a list 0, ..., n)
microbial_species = [Int(i) for i=1:n_microbial_species];
otu_id = ["OTU " * string(i) for i in microbial_species];

# define the source community of the neutral model
effector_in_source = 0.1
source_community =[(1-effector_in_source)/(n_microbial_species-1) for i=1:n_microbial_species]; #We force the source to be homogeneous
source_community[effector_microbe] = effector_in_source
source_community
#source_community = 1.0*source_community/sum(source_community)

function sim_herit_var(herit)
# function that runs the simulation
    
    # define the hosts
    hosts = Matrix{Int64}(n_microbial_species,n_hosts)
    host_vec = Vector{Int64}(n_hosts)
    host_id = Array{String,1}(n_hosts)
    tmp_host_matrix = Matrix{Int64}(n_microbial_species,n_hosts)
    
    for host_num=1:n_hosts
        hosts[:,host_num] = colonize_from_environment(microbial_species, n_microbes, source_community, n_initial_colonizers)
        host_vec[host_num] = host_num
        host_id[host_num] = "Host " * string(host_num)
    end

    
    microbiome_heritability = herit
    for t = 1:t_max

        # host reproduction
        if host_reproduction && t % host_generation_time == 0

            # pick the new set of hosts with the probabilities of the previous
            parents_vector = reproduction_impact == 0 ? sample(host_vec,n_hosts): wsample(host_vec,
            set_host_weight_vector(reproduction_impact,reproduction_additivity,effector_microbe,hosts),n_hosts)

            for new_id in host_vec
                # determine microbiome for new host as weighted sum of environmental and ancestral microbes
                env_sample = 1.0*colonize_from_environment(microbial_species, n_microbes, source_community, n_initial_colonizers)/n_microbes  
            inherited_microbes = wsample(microbial_species, (1.0*microbiome_heritability*hosts[:,parents_vector[new_id]]/n_microbes + (1 - microbiome_heritability)*env_sample),n_microbes)

                tmp_host_matrix[:,new_id] = zeros(Int,n_microbial_species)
                for species in inherited_microbes
                    tmp_host_matrix[species,new_id] += 1
                end
            end

        hosts = copy(tmp_host_matrix)
        end


        # microbial reproduction
        for host_index= 1:n_hosts
            # death
            hosts[wsample(microbial_species, 1.0*hosts[:,host_index]/n_microbes),host_index] -= 1
            # birth
            hosts[wsample(microbial_species, (m*source_community + (1-m)*hosts[:,host_index]/(n_microbes-1))),host_index] += 1
        end

    end
    return hosts
end

herit_range = 0:0.01:1
reps = 1

out_data = Array{Int64,3}(reps,length(herit_range),n_hosts)

for i in 1:length(herit_range)
    println("Herit: ",herit_range[i])
    for rep in 1:reps
        out_data[rep,i,:] = sim_herit_var(herit_range[i])[1,:]
        println(out_data[rep,i,:])
    end
end

using DataFrames,Gadfly,CSV

means = mapslices(mean,out_data,(1,3))[:]
stds = mapslices(std,out_data,(1,3))[:]
df_sym = DataFrame(
Herit = herit_range,
means = means[:],
stds = stds[:],
ymin = means[:] .- stds[:]/sqrt(n_hosts*reps),
ymax = means[:] .+ stds[:]/sqrt(n_hosts*reps)
) 

CSV.write("Her_sensibility_dataset.csv",df_sym)


p = plot(df_sym, 
    x=:Herit, y=:means, 
    ymin=:ymin, ymax=:ymax, 
    Guide.title("Host Generation Time and Heritability"),
Guide.xlabel("Microbiome Heritability"),
Guide.xticks(ticks=[0:0.1:1;]),
Guide.ylabel("Average Symbiont per host"),
    Geom.line, Geom.ribbon)

draw(SVG("Herit_Sen.svg",30cm,30cm),p)
draw(PNG("Herit_Sen.png",30cm,30cm),p)
