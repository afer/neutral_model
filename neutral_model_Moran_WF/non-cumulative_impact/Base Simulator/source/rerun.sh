#Bash script to rerun a previously ran simulation.

echo "Starting simulation"
/usr/bin/time -o "PERFORMANCE/neut_sim_time.txt" julia SOURCE/neutral_simulation.jl

echo "Simulation finished.Starting Plotting."

python2 SOURCE/neutral_fit.py 0 400 1 OUTPUT/otutable_sampled_t*.csv

echo "Plotting finished. Files in $WDIR/OUTPUT"
