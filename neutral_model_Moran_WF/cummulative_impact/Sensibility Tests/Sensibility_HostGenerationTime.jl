workingpath="runs/"*Dates.format(now(), "yyyymmdd_HHMMSS")*"_Sensibility_HostGenerationTime"
try mkdir(workingpath) catch end
try cp("Sensibility_HostGenerationTime.jl",workingpath*"/Sensibility_HostGenerationTime.jl") catch end

using StatsBase,Distributions, DataFrames, Gadfly, CSV

seed = "NA" # seed for repeated initial conditions
#srand(seed)
 
const n_hosts = 5                      # number of hosts
const n_microbes = 100                # number of individuals (microbes) per host
const n_microbial_species = 10         # number of species
const host_reproduction = true          # do hosts also reproduce? "false" corresponds to the standard neutral model
const n_initial_colonizers = 5          # how many microbial species colonize a host from the environment?
const m = 0.01    # what fraction of the microbiome is inherited?

const t_max = 1e7                   # how long does the simulation run

const reproduction_additivity = true   # multiple bacteria of the effector type will keep affecting reproduction if true. If this is true reproduction impact should be very low.
const reproduction_impact = 0.5      #(alpha_r) Impact of the reproduction in the selection [value between -1 and 1]
const effector_microbe = 1              # which microbe has an effect in the host

# define the source community 
source_community =[1/n_microbial_species for i=1:n_microbial_species]; 

# function takes a range of species and their distribution in an environment and returns a microbiome
function colonize_from_environment(species::Array{Int64}, n_microbes::Int64, environment::Array{Float64}, n_colonizers::Int64)
    microbiome = zeros(Int64,length(species))

    init_species = wsample(species, environment, n_colonizers)
    for i = 1:length(init_species)-1
        if n_microbes - sum(microbiome) > 1
            microbiome[init_species[i]] += sample(1:n_microbes - sum(microbiome))
        end
    end
    microbiome[init_species[length(init_species)]] += n_microbes - sum(microbiome)

    return microbiome
end

# function takes the impact of one strain, if this impact is additive, the index of this strain and the state of the system and returns a weight vector
function set_host_weight_vector(effect_value::Float64,additivity::Bool, effector_microbe::Int64,symbiont_accumulation::Array{Int64,1})
    new_n_hosts = length(symbiont_accumulation)

        effector_microbes_per_host_w =  effect_value > 0 ? symbiont_accumulation : n_microbes - symbiont_accumulation
        total_effectors_w = sum(symbiont_accumulation)

        if sum(total_effectors_w) > 0    #This condition is for the case where there are no effector microbes.
            effect_type = additivity == true ? effector_microbes_per_host_w :  effector_microbes_per_host_w .!= 0
            abs_effect_value = abs(effect_value)
            hosts_weight_vector = (1. / new_n_hosts) * (1. - abs_effect_value) + abs_effect_value * (effect_type / total_effectors_w )
        else
            hosts_weight_vector = ones(new_n_hosts) / new_n_hosts
        end

    return hosts_weight_vector
end

function sim_herit_var(herit::Float64,HGT::Int64)

    host_generation_time = HGT
    
     # define the hosts
    hosts = Matrix{Int64}(n_microbial_species,n_hosts)
    host_vec = Vector{Int64}(n_hosts)
    host_id = Array{String,1}(n_hosts)
    tmp_host_matrix = Matrix{Int64}(n_microbial_species,n_hosts)
    
    for host_num=1:n_hosts
        hosts[:,host_num] = colonize_from_environment(microbial_species, n_microbes, source_community, n_initial_colonizers)
        host_vec[host_num] = host_num
        host_id[host_num] = "Host " * string(host_num)
    end
 
    # define the host accumulation vector
    symbiont_cummulative_vector = copy(hosts[effector_microbe,:])
    microbiome_heritability = herit
    
    println("HGT: ", HGT)
    
    # run the simulation
    for t = 1:t_max

        # host reproduction
        if host_reproduction && t % host_generation_time == 0

            # pick the new set of hosts with the probabilities of the previous
            parents_vector = reproduction_impact == 0 ? sample(host_vec,n_hosts): wsample(host_vec,
            set_host_weight_vector(reproduction_impact,reproduction_additivity,effector_microbe,symbiont_cummulative_vector),n_hosts)

            for new_id in host_vec
                # determine microbiome for new host as weighted sum of environmental and ancestral microbes
                env_sample = 1.0*colonize_from_environment(microbial_species, n_microbes, source_community, n_initial_colonizers)/n_microbes  
            inherited_microbes = wsample(microbial_species, (1.0*microbiome_heritability*hosts[:,parents_vector[new_id]]/n_microbes + (1. - microbiome_heritability)*env_sample),n_microbes)

                tmp_host_matrix[:,new_id] = zeros(Int,n_microbial_species)
                for species in inherited_microbes
                    tmp_host_matrix[species,new_id] += 1
                end
            end

        hosts = copy(tmp_host_matrix)
        symbiont_cummulative_vector = copy(hosts[effector_microbe,:])
        end

        # microbial reproduction
        for host_index= 1:n_hosts
            # death
            hosts[wsample(microbial_species, 1.0*hosts[:,host_index]/n_microbes),host_index] -= 1
            # birth
            hosts[wsample(microbial_species, (m*source_community + (1-m)*hosts[:,host_index]/(n_microbes-1))),host_index] += 1
        end
        symbiont_cummulative_vector += hosts[effector_microbe,:]
    
    end
    return hosts
end

### Simulation start

# define array of the species (basically just a list 0, ..., n)
microbial_species = [Int(i) for i=1:n_microbial_species];
otu_id = ["OTU " * string(i) for i in microbial_species];

# Sensibility parameters
const HGT_range = [10^i for i=1:3] # immigration probability of the neutral model
const herit_range = 0:0.01:1 # heritability values
const reps = 5 #Number of repetitions per value of heritability and source abundance

df_sym = DataFrame(Herit=Float64[],HGT=Int64[],means=Float64[],stderrs = Float64[],ymin=Float64[],ymax=Float64[])
reps_matrix = Array{Int64,2}(reps,n_hosts)

for parameter in HGT_range
    println("HGT: ",parameter)

    for i in 1:length(herit_range) 
        for rep in 1:reps
            reps_matrix[rep,:] = sim_herit_var(herit_range[i],parameter)[1,:]
        end
        
        sim_mean, sim_stderrs = (mean(reps_matrix),std(reps_matrix)/sqrt(length(reps_matrix)))
        println(sim_mean,"\t",sim_stderrs)

        push!(df_sym,[herit_range[i],parameter,sim_mean,sim_stderrs,sim_mean-sim_stderrs,sim_mean+sim_stderrs])
    end
end

CSV.write(workingpath*"/HGT_sensibility_dataset.csv",df_sym)

p = plot(df_sym, 
x=:Herit, y=:means, 
    ymin=:ymin, ymax=:ymax,
    color=:HGT,
Guide.title("Host Generation Time effect on Heritability curve"),
Guide.xlabel("Microbiome heritability"),
Guide.xticks(ticks=[0:0.1:1;]),
Guide.ylabel("Average symbiont per host"),
Guide.yticks(ticks=[0:Int(n_microbes/10):n_microbes;]),
Guide.colorkey(title="Host Generation Time"),
    Geom.line, Geom.ribbon)

draw(SVG(workingpath*"/HGT_on_Heritability_Curve.svg",20cm,20cm),p)
